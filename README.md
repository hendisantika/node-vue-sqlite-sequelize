# node-vue-sqlite-sequelize

In this tutorial I will be demonstrating how to build a simple contacts management web application using Node.js, Express.js, Vue.js in conjunction with the sequelize.js object relational mapper (ORM) backed by a SQLite database.

### Usage

1. Clone repo

   git clone https://gitlab.com/hendisantika/node-vue-sqlite-sequelize.git

2. install dependencies (Built with Node.js version 8.10)
    ```javascript
    cd node-vue-sqlite-sequelize/
    npm install

    ```
    
3. run migrations and seeders
   ```javascript
    node_modules/.bin/sequelize db:migrate
    node_modules/.bin/sequelize db:seed:all
       
    ``` 
    
4. start express server
    
    `npm start`

### Screenshot

Home Page

![Home Page](img/home.png "Home Page")

Update Page

![Update Page](img/update.png "Update Page")